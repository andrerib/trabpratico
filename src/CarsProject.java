
import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.CardLayout;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import javax.xml.transform.Result;
import java.awt.Insets;
import java.sql.Connection;
import java.sql.Date;
import java.sql.*;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Component;
import java.awt.*;
import javax.swing.Box;
import javax.imageio.ImageWriteParam;
import javax.swing.*;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CarsProject {

	static final String JDBC_DRIVER = "org.sqlite.JDBC";
	static final String DB_URL = "jdbc:sqlite:CarProject.sqlite";
	private Connection conn = null;
	private Statement stmt = null;

	private JFrame frame;
	private JTextField userField;
	private JPasswordField passField;
	private JButton btnLogin;
	private Component horizontalStrut;
	private Component horizontalStrut_1;
	private JLabel lblUsername;
	private JLabel lblPassword;
	private Component horizontalStrut_2;
	private Component horizontalStrut_3;
	private Component verticalStrut;
	private JButton btnRegister;
	private JButton btnAnonymous;
	private JPanel Anon;
	private JPanel Main;
	private JPanel Detalhes;
	private JPanel Adicionar;
	private JPanel Imposto;
	private JLabel lblRegister;
	private JTextField regUser;
	private JTextField regPass;
	private JLabel lblUsername_1;
	private JLabel lblPassword_1;
	private JButton btnRegister_1;
	private JButton btnBackRegister;
	private Component horizontalStrut_4;
	private Component verticalStrut_1;
	private JLabel lblListaDosCarros;
	private JList list;
	DefaultListModel<Car> listModel;
	DefaultListModel<Revisao> listModelRevisoes;
	DefaultListModel<Imposto> listModelImposto;
	DefaultListModel<Inspecao> listModelInspecao;
	DefaultListModel<Abastecimento> listModelAbastecimento;
	DefaultComboBoxModel<Combustivel> listModelComb;
	private JLabel lblNome;
	private JTextField regNome;
	private Component horizontalStrut_5;
	private Component verticalStrut_2;
	String saveUser;
	private JList carList_1;
	private JLabel lblNomeDoCarro;
	private JLabel lblMarca;
	private JLabel lblModelo;
	private JLabel lblMatricula;
	private JLabel lblCor;
	private JLabel lblCilindrada;
	private JTextField cilindradaField;
	private JTextField corField;
	private JTextField matriculaField;
	private JTextField modeloField;
	private JTextField marcaField;
	private JTextField aAquisicaoField;
	private JTextField pAquisicaoField;
	private JTextField aVendaField;
	private JTextField pVendaField;
	private JButton btnDetalhes;
	private JButton btnDetalheMain;
	private JButton btnAlterarDetalhes;
	private JComboBox comboBoxCombustivel;
	String mtrc;
	private JPanel Inspecao;
	private JPanel Abastecimento;
	private JPanel Revisoes;
	private JTextField addMarcaField;
	private JTextField addModeloField;
	private JTextField addMatField;
	private JTextField addCorField;
	private JTextField addCilindradaField;
	private JTextField addAnoAquiField;
	private JTextField addPrecoAquiField;
	private JTextField addAnoVendaField;
	private JTextField addPrecoVendaField;
	private JTextField idRevField;
	private JTextField matRevField;
	private JTextField dataRevField;
	private JTextField oficinaRevField;
	private JTextField addDataRevField;
	private JTextField addOficinaRevField;
	private JLabel lblNewLabel_19;
	private JList impostoList;
	private JButton btnDetImposto;
	private JButton btnAdicionarImposto;
	private JButton btnImpostoMain;
	private JPanel DetalhesImposto;
	private JPanel AdicionarImposto;
	private JPanel DetalhesInspecao;
	private JPanel AdicionarInspecao;
	private JPanel DetalhesAbastecimento;
	private JPanel AdicionarAbastecimento;
	private JLabel lblNewLabel_20;
	private JLabel lblNewLabel_21;
	private JLabel lblNewLabel_22;
	private JLabel lblNewLabel_23;
	private JLabel lblNewLabel_24;
	private JTextField idImpostoField;
	private JTextField matImpostoField;
	private JTextField dataImpostoField;
	private JTextField valorImpostoField;
	private JButton btnAlterarImposto;
	private JButton btnDetImpostoImposto;
	private JLabel lblNewLabel_25;
	private JLabel lblNewLabel_26;
	private JLabel lblNewLabel_27;
	private JTextField addDataImpostoField;
	private JTextField addValorImpostoField;
	private JButton btnAddImposto;
	private JButton btwAddImpostoImposto;
	private JList inspecaoList;
	private JButton btnDetInspecao;
	private JButton btnAdicionarInspecao;
	private JButton btnInspecaoMain;
	private JLabel lblDetalhes;
	private JLabel label_1;
	private JTextField idInspecaoField;
	private JButton btnAlterarInspecao;
	private JTextField matInspecaoField;
	private JLabel label_2;
	private JLabel label_3;
	private JTextField dataInspecaoField;
	private JTextField valorInspecaoField;
	private JLabel label_4;
	private JButton btnDetInspecaoInspecao;
	private JLabel lblAdicionarNovaInspeo;
	private JLabel lblData_1;
	private JTextField addDataInspecaoField;
	private JButton btwAddInspecao;
	private JTextField addValorInspecaoField;
	private JLabel lblValor_1;
	private JButton btwAddInspecaoInspecao;
	private JList abastecimentoList;
	private JButton btnDetAbastecimento;
	private JButton btnAdicionarAbastecimento;
	private JButton btnAbastecimentoMain;
	private JLabel lblNewLabel_28;
	private JLabel lblNewLabel_29;
	private JLabel lblNewLabel_30;
	private JLabel lblNewLabel_31;
	private JLabel lblNewLabel_32;
	private JTextField idAbastField;
	private JTextField matAbastField;
	private JTextField localAbastField;
	private JTextField kmsAbastField;
	private JLabel lblLitros;
	private JTextField litrosAbastField;
	private JTextField dataAbastField;
	private JLabel lblData;
	private JLabel lblValor;
	private JTextField valorAbastField;
	private JButton btnDetAbastAbast;
	private JButton btnAlterarAbast;
	private JLabel label_8;
	private JTextField addLocalAbastField;
	private JLabel label_9;
	private JTextField addKmsAbastField;
	private JLabel label_10;
	private JTextField addLitrosAbastField;
	private JTextField addDataAbastField;
	private JLabel label_11;
	private JLabel label_12;
	private JTextField addValorAbastField;
	private JLabel lblNewLabel_33;
	private JButton btnAddAbast;
	private JButton btnAddAbastAbast;
	private JLabel lblNewLabel_34;
	private JLabel lblAbastecimentos;
	private JButton btnRevisoes;
	private JList revisoesList;
	private JButton btnRevisoesMain;
	private JButton btnDetalhesRev;
	private JButton btnDetRevRev;
	private JButton btnAlterarDetRev;
	private JButton btnAdicionarRev;
	private JButton btnAddRevRev;
	private JButton btnAddRev;
	private JButton btnImposto;
	private JButton btnInspecao;
	private JButton btnAbastecimento;
	private JButton btnAdicionar;
	private JButton btnAdicionarMain;
	private JComboBox comboBoxAddCarroComb;
	private JButton btnAdicionarCarro;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CarsProject window = new CarsProject();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CarsProject() {
		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setBounds(100, 100, 500, 360);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));

		JPanel DetalhesRevisoes = new JPanel();
		JPanel AdicionarRevisoes = new JPanel();
		JPanel Login = new JPanel();
		frame.getContentPane().add(Login, "name_58431136616843");
		GridBagLayout gbl_Login = new GridBagLayout();
		gbl_Login.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_Login.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_Login.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_Login.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		Login.setLayout(gbl_Login);

		JPanel Register = new JPanel();
		frame.getContentPane().add(Register, "name_58432927726470");
		GridBagLayout gbl_Register = new GridBagLayout();
		gbl_Register.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_Register.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_Register.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_Register.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		Register.setLayout(gbl_Register);

		verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut.gridx = 7;
		gbc_verticalStrut.gridy = 1;
		Login.add(verticalStrut, gbc_verticalStrut);

		JLabel lblLogin = new JLabel("login");
		GridBagConstraints gbc_lblLogin = new GridBagConstraints();
		gbc_lblLogin.insets = new Insets(0, 0, 5, 5);
		gbc_lblLogin.gridx = 7;
		gbc_lblLogin.gridy = 2;
		Login.add(lblLogin, gbc_lblLogin);

		horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 4;
		gbc_horizontalStrut_2.gridy = 3;
		Login.add(horizontalStrut_2, gbc_horizontalStrut_2);

		lblUsername = new JLabel("Username");
		GridBagConstraints gbc_lblUsername = new GridBagConstraints();
		gbc_lblUsername.insets = new Insets(0, 0, 5, 5);
		gbc_lblUsername.anchor = GridBagConstraints.EAST;
		gbc_lblUsername.gridx = 6;
		gbc_lblUsername.gridy = 3;
		Login.add(lblUsername, gbc_lblUsername);

		userField = new JTextField();
		GridBagConstraints gbc_userField = new GridBagConstraints();
		gbc_userField.insets = new Insets(0, 0, 5, 5);
		gbc_userField.fill = GridBagConstraints.HORIZONTAL;
		gbc_userField.gridx = 7;
		gbc_userField.gridy = 3;
		Login.add(userField, gbc_userField);
		userField.setColumns(10);

		horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 8;
		gbc_horizontalStrut.gridy = 3;
		Login.add(horizontalStrut, gbc_horizontalStrut);

		horizontalStrut_3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_3 = new GridBagConstraints();
		gbc_horizontalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_3.gridx = 5;
		gbc_horizontalStrut_3.gridy = 4;
		Login.add(horizontalStrut_3, gbc_horizontalStrut_3);

		lblPassword = new JLabel("Password");
		GridBagConstraints gbc_lblPassword = new GridBagConstraints();
		gbc_lblPassword.insets = new Insets(0, 0, 5, 5);
		gbc_lblPassword.anchor = GridBagConstraints.EAST;
		gbc_lblPassword.gridx = 6;
		gbc_lblPassword.gridy = 4;
		Login.add(lblPassword, gbc_lblPassword);

		passField = new JPasswordField();
		GridBagConstraints gbc_passField = new GridBagConstraints();
		gbc_passField.insets = new Insets(0, 0, 5, 5);
		gbc_passField.fill = GridBagConstraints.HORIZONTAL;
		gbc_passField.gridx = 7;
		gbc_passField.gridy = 4;
		Login.add(passField, gbc_passField);

		horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 9;
		gbc_horizontalStrut_1.gridy = 4;
		Login.add(horizontalStrut_1, gbc_horizontalStrut_1);

		try{
			Class.forName(JDBC_DRIVER);
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL);
			System.out.println("Quering database...");
			stmt = conn.createStatement();

			listModel = new DefaultListModel<Car>();
			fillList("SELECT * FROM Cars");
			list = new JList(listModel);

		}catch(Exception ee){

		};

		btnLogin = new JButton("Login");
		btnLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					Class.forName(JDBC_DRIVER);
					System.out.println("Connecting to database...");
					conn = DriverManager.getConnection(DB_URL);
					System.out.println("Quering database...");
					stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery("SELECT * FROM Users");
					int veriPassUser=0;
					while (rs.next())
					{
						String user = rs.getString(1);
						String pass = rs.getString(2);
						if(userField.getText().equals(user) )
						{
							saveUser=user;
							if((pass.equals(new String((passField.getPassword())))))
							{
								veriPassUser=1;				   				
							}else
								veriPassUser=2;
						}
					}
					if(veriPassUser==1)
					{
						Login.setVisible(false);
						Main.setVisible(true);		    			   
						fillList("SELECT * FROM Cars WHERE Username LIKE '"+saveUser+"'");
					}else if(veriPassUser==2)
					{
						JOptionPane.showMessageDialog(frame, "Password errada");
					}
					else
					{
						JOptionPane.showMessageDialog(frame, "O usename introduzido n�o existe");
					}

					System.out.println("Database queried successfully...");
				} catch (SQLException se) {
					// Handle errors for JDBC
					se.printStackTrace();
				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}


			}
		});
		GridBagConstraints gbc_btnLogin = new GridBagConstraints();
		gbc_btnLogin.insets = new Insets(0, 0, 5, 5);
		gbc_btnLogin.gridx = 7;
		gbc_btnLogin.gridy = 5;
		Login.add(btnLogin, gbc_btnLogin);

		btnAnonymous = new JButton("Anonymous");
		btnAnonymous.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				Login.setVisible(false);
				Anon.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnAnonymous = new GridBagConstraints();
		gbc_btnAnonymous.insets = new Insets(0, 0, 0, 5);
		gbc_btnAnonymous.gridx = 6;
		gbc_btnAnonymous.gridy = 6;
		Login.add(btnAnonymous, gbc_btnAnonymous);

		btnRegister = new JButton("Register");
		btnRegister.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				Login.setVisible(false);
				Register.setVisible(true);

			}
		});
		GridBagConstraints gbc_btnRegister = new GridBagConstraints();
		gbc_btnRegister.insets = new Insets(0, 0, 0, 5);
		gbc_btnRegister.gridx = 7;
		gbc_btnRegister.gridy = 6;
		Login.add(btnRegister, gbc_btnRegister);

		verticalStrut_1 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_1 = new GridBagConstraints();
		gbc_verticalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_1.gridx = 6;
		gbc_verticalStrut_1.gridy = 0;
		Register.add(verticalStrut_1, gbc_verticalStrut_1);



		lblRegister = new JLabel("Register");
		GridBagConstraints gbc_lblRegister = new GridBagConstraints();
		gbc_lblRegister.insets = new Insets(0, 0, 5, 5);
		gbc_lblRegister.gridx = 6;
		gbc_lblRegister.gridy = 1;
		Register.add(lblRegister, gbc_lblRegister);

		lblUsername_1 = new JLabel("Username");
		GridBagConstraints gbc_lblUsername_1 = new GridBagConstraints();
		gbc_lblUsername_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblUsername_1.anchor = GridBagConstraints.EAST;
		gbc_lblUsername_1.gridx = 5;
		gbc_lblUsername_1.gridy = 3;
		Register.add(lblUsername_1, gbc_lblUsername_1);

		regUser = new JTextField();
		GridBagConstraints gbc_regUser = new GridBagConstraints();
		gbc_regUser.insets = new Insets(0, 0, 5, 5);
		gbc_regUser.fill = GridBagConstraints.HORIZONTAL;
		gbc_regUser.gridx = 6;
		gbc_regUser.gridy = 3;
		Register.add(regUser, gbc_regUser);
		regUser.setColumns(10);

		lblPassword_1 = new JLabel("Password");
		GridBagConstraints gbc_lblPassword_1 = new GridBagConstraints();
		gbc_lblPassword_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblPassword_1.anchor = GridBagConstraints.EAST;
		gbc_lblPassword_1.gridx = 5;
		gbc_lblPassword_1.gridy = 4;
		Register.add(lblPassword_1, gbc_lblPassword_1);

		regPass = new JTextField();
		GridBagConstraints gbc_regPass = new GridBagConstraints();
		gbc_regPass.insets = new Insets(0, 0, 5, 5);
		gbc_regPass.fill = GridBagConstraints.HORIZONTAL;
		gbc_regPass.gridx = 6;
		gbc_regPass.gridy = 4;
		Register.add(regPass, gbc_regPass);
		regPass.setColumns(10);

		lblNome = new JLabel("Nome");
		GridBagConstraints gbc_lblNome = new GridBagConstraints();
		gbc_lblNome.anchor = GridBagConstraints.EAST;
		gbc_lblNome.insets = new Insets(0, 0, 5, 5);
		gbc_lblNome.gridx = 5;
		gbc_lblNome.gridy = 5;
		Register.add(lblNome, gbc_lblNome);

		regNome = new JTextField();
		GridBagConstraints gbc_regNome = new GridBagConstraints();
		gbc_regNome.insets = new Insets(0, 0, 5, 5);
		gbc_regNome.fill = GridBagConstraints.HORIZONTAL;
		gbc_regNome.gridx = 6;
		gbc_regNome.gridy = 5;
		Register.add(regNome, gbc_regNome);
		regNome.setColumns(10);

		horizontalStrut_4 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_4 = new GridBagConstraints();
		gbc_horizontalStrut_4.insets = new Insets(0, 0, 0, 5);
		gbc_horizontalStrut_4.gridx = 3;
		gbc_horizontalStrut_4.gridy = 6;
		Register.add(horizontalStrut_4, gbc_horizontalStrut_4);

		btnBackRegister = new JButton("<");
		btnBackRegister.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				regUser.setText("");
				regPass.setText("");
				regNome.setText("");
				Register.setVisible(false);
				Login.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnBackRegister = new GridBagConstraints();
		gbc_btnBackRegister.insets = new Insets(0, 0, 0, 5);
		gbc_btnBackRegister.gridx = 4;
		gbc_btnBackRegister.gridy = 6;
		Register.add(btnBackRegister, gbc_btnBackRegister);

		btnRegister_1 = new JButton("Register");
		btnRegister_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try{
					Class.forName(JDBC_DRIVER);
					System.out.println("Connecting to database...");
					conn = DriverManager.getConnection(DB_URL);
					System.out.println("Quering database...");
					stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery("SELECT * FROM Users");
					int veriUser=0;
					while(rs.next()){
						String user = rs.getString(1);
						if(regUser.getText().equals(user) )
							veriUser++;
					}
					if(!regUser.getText().isEmpty() && !regPass.getText().isEmpty() && !regNome.getText().isEmpty() && veriUser==0){
						stmt.executeUpdate("INSERT INTO Users " + "VALUES ('"+regUser.getText()+"', '"+regPass.getText()+"', '"+regNome.getText()+"')");
						regUser.setText("");
						regPass.setText("");
						regNome.setText("");
						Register.setVisible(false);
						Login.setVisible(true);
					}
					else if(veriUser==0)
						JOptionPane.showMessageDialog(frame, "Preencha todos os campos");
					else
						JOptionPane.showMessageDialog(frame, "O username introduzido j� existe");

				}catch (SQLException se) {
					// Handle errors for JDBC
					se.printStackTrace();
				} catch (Exception fe) {
					// Handle errors for Class.forName
					fe.printStackTrace();
				}
			}
		});
		GridBagConstraints gbc_btnRegister_1 = new GridBagConstraints();
		gbc_btnRegister_1.insets = new Insets(0, 0, 0, 5);
		gbc_btnRegister_1.gridx = 6;
		gbc_btnRegister_1.gridy = 6;
		Register.add(btnRegister_1, gbc_btnRegister_1);

		Anon = new JPanel();
		frame.getContentPane().add(Anon, "name_57202989102579");
		GridBagLayout gbl_Anon = new GridBagLayout();
		gbl_Anon.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_Anon.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_Anon.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_Anon.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		Anon.setLayout(gbl_Anon);

		lblListaDosCarros = new JLabel("Lista dos Carros");
		GridBagConstraints gbc_lblListaDosCarros = new GridBagConstraints();
		gbc_lblListaDosCarros.insets = new Insets(0, 0, 5, 0);
		gbc_lblListaDosCarros.gridx = 7;
		gbc_lblListaDosCarros.gridy = 0;
		Anon.add(lblListaDosCarros, gbc_lblListaDosCarros);

		verticalStrut_2 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_2 = new GridBagConstraints();
		gbc_verticalStrut_2.insets = new Insets(0, 0, 5, 0);
		gbc_verticalStrut_2.gridx = 7;
		gbc_verticalStrut_2.gridy = 2;
		Anon.add(verticalStrut_2, gbc_verticalStrut_2);

		horizontalStrut_5 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_5 = new GridBagConstraints();
		gbc_horizontalStrut_5.insets = new Insets(0, 0, 0, 5);
		gbc_horizontalStrut_5.gridx = 6;
		gbc_horizontalStrut_5.gridy = 3;
		Anon.add(horizontalStrut_5, gbc_horizontalStrut_5);

		GridBagConstraints gbc_list = new GridBagConstraints();
		gbc_list.fill = GridBagConstraints.BOTH;
		gbc_list.gridx = 7;
		gbc_list.gridy = 3;
		Anon.add(list, gbc_list);

		Main = new JPanel();
		frame.getContentPane().add(Main, "name_57225448007923");
		Main.setLayout(null);

		JLabel lblListaDeCarros = new JLabel("Lista de carros");
		lblListaDeCarros.setBounds(137, 11, 129, 14);
		Main.add(lblListaDeCarros);

		carList_1 = new JList(listModel);
		carList_1.setBounds(36, 58, 238, 190);
		Main.add(carList_1);

		btnDetalhes = new JButton("Detalhes");
		btnDetalhes.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				if(carList_1.getSelectedValue()==null)
					JOptionPane.showMessageDialog(frame, "Selecione um carro");
				else{
					Main.setVisible(false);
					Detalhes.setVisible(true);
					Car carSelec = (Car) carList_1.getSelectedValue();
					lblNomeDoCarro.setText(carSelec.toString());
					marcaField.setText(carSelec.getMarca());
					modeloField.setText(carSelec.getModelo());
					matriculaField.setText(carSelec.getMatricula());
					corField.setText(carSelec.getCor());
					cilindradaField.setText(""+carSelec.getCilindrada());
					aAquisicaoField.setText(""+carSelec.getAnoAquisicao());
					pAquisicaoField.setText(""+carSelec.getPrecoAquisicao());
					aVendaField.setText(""+carSelec.getAnoVenda());
					pVendaField.setText(""+carSelec.getPrecoVenda());
					marcaField.setEditable(false);
					modeloField.setEditable(false);
					matriculaField.setEditable(false);
					corField.setEditable(false);
					cilindradaField.setEditable(false);
					aAquisicaoField.setEditable(false);
					pAquisicaoField.setEditable(false);
					aVendaField.setEditable(false);
					pVendaField.setEditable(false);
					comboBoxCombustivel.setEnabled(false);
				}}
		});
		btnDetalhes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnDetalhes.setBounds(284, 55, 135, 23);
		Main.add(btnDetalhes);

		listModelRevisoes = new DefaultListModel<Revisao>();

		btnRevisoes = new JButton("Revis\u00F5es");
		btnRevisoes.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				if(carList_1.getSelectedValue()==null)
					JOptionPane.showMessageDialog(frame, "Selecione um carro");
				else{
					Main.setVisible(false);
					Revisoes.setVisible(true);
					listModelRevisoes.clear();
					try{
						Car carSelec = (Car) carList_1.getSelectedValue();
						String matr=carSelec.getMatricula();
						ResultSet rs = stmt.executeQuery("SELECT * FROM Revisao  WHERE Matricula LIKE '"+matr+"'");
						while(rs.next()){
							Revisao rev = new Revisao(
									rs.getInt("idRevisao"),
									rs.getString("matricula"),
									rs.getString("oficina"),
									rs.getString("data"));
							listModelRevisoes.addElement(rev);
						}
					}catch(Exception ee){}
				}

			}
		});
		btnRevisoes.setBounds(284, 89, 135, 23);
		Main.add(btnRevisoes);
		listModelImposto = new DefaultListModel<Imposto>();
		btnImposto = new JButton("Imposto");
		btnImposto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if(carList_1.getSelectedValue()==null)
					JOptionPane.showMessageDialog(frame, "Selecione um carro");
				else{
					Main.setVisible(false);
					Imposto.setVisible(true);
					listModelImposto.clear();
					try{
						Car carSelec = (Car) carList_1.getSelectedValue();
						String matr=carSelec.getMatricula();
						ResultSet rs = stmt.executeQuery("SELECT * FROM Imposto  WHERE Matricula LIKE '"+matr+"'");
						while(rs.next()){
							Imposto imp = new Imposto(
									rs.getInt("idImposto"),
									rs.getString("matricula"),
									rs.getDouble("valor"),
									rs.getString("data"));
							listModelImposto.addElement(imp);
						}
					}catch(Exception ee){}
				}
			}
		});
		btnImposto.setBounds(284, 123, 135, 23);
		Main.add(btnImposto);

		listModelInspecao = new DefaultListModel<Inspecao>();
		btnInspecao = new JButton("Inspe\u00E7\u00E3o");
		btnInspecao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if(carList_1.getSelectedValue()==null)
					JOptionPane.showMessageDialog(frame, "Selecione um carro");
				else{
					Main.setVisible(false);
					Inspecao.setVisible(true);
					listModelInspecao.clear();
					try{
						Car carSelec = (Car) carList_1.getSelectedValue();
						String matr=carSelec.getMatricula();
						ResultSet rs = stmt.executeQuery("SELECT * FROM Inspecao  WHERE Matricula LIKE '"+matr+"'");
						while(rs.next()){
							Inspecao insp = new Inspecao(
									rs.getInt("idInspecao"),
									rs.getString("matricula"),
									rs.getDouble("valor"),
									rs.getString("data"));
							listModelInspecao.addElement(insp);
						}
					}catch(Exception ee){}
				}
			}
		});
		btnInspecao.setBounds(284, 157, 135, 23);
		Main.add(btnInspecao);
		listModelAbastecimento = new DefaultListModel<Abastecimento>();
		btnAbastecimento = new JButton("Abastecimento");
		btnAbastecimento.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if(carList_1.getSelectedValue()==null)
					JOptionPane.showMessageDialog(frame, "Selecione um carro");
				else{
					Main.setVisible(false);
					Abastecimento.setVisible(true);
					listModelAbastecimento.clear();
					try{
						Car carSelec = (Car) carList_1.getSelectedValue();
						String matr=carSelec.getMatricula();
						ResultSet rs = stmt.executeQuery("SELECT * FROM Abastecimento  WHERE Matricula LIKE '"+matr+"'");
						while(rs.next()){
							Abastecimento abast = new Abastecimento(
									rs.getInt("idAbastecimento"),
									rs.getString("matricula"),
									rs.getString("local"),
									rs.getDouble("kMs"),
									rs.getDouble("litros"),
									rs.getDouble("valor"),
									rs.getString("data"));
							listModelAbastecimento.addElement(abast);
						}
					}catch(Exception ee){}
				}
			}
		});
		btnAbastecimento.setBounds(284, 191, 135, 23);
		Main.add(btnAbastecimento);

		btnAdicionar = new JButton("Adicionar");
		btnAdicionar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Main.setVisible(false);
				Adicionar.setVisible(true);
			}
		});
		btnAdicionar.setBounds(284, 225, 135, 23);
		Main.add(btnAdicionar);

		Detalhes = new JPanel();
		frame.getContentPane().add(Detalhes, "name_57227618481224");
		Detalhes.setLayout(null);

		lblNomeDoCarro = new JLabel("Detalhes");
		lblNomeDoCarro.setBounds(21, 11, 236, 14);
		Detalhes.add(lblNomeDoCarro);

		lblMarca = new JLabel("Marca");
		lblMarca.setBounds(21, 57, 46, 14);
		Detalhes.add(lblMarca);

		lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(21, 98, 46, 14);
		Detalhes.add(lblModelo);

		lblMatricula = new JLabel("Matricula");
		lblMatricula.setBounds(21, 136, 73, 14);
		Detalhes.add(lblMatricula);

		lblCor = new JLabel("Cor");
		lblCor.setBounds(21, 174, 46, 14);
		Detalhes.add(lblCor);

		lblCilindrada = new JLabel("Cilindrada");
		lblCilindrada.setBounds(21, 214, 73, 14);
		Detalhes.add(lblCilindrada);

		cilindradaField = new JTextField();
		cilindradaField.setBounds(104, 211, 86, 20);
		Detalhes.add(cilindradaField);
		cilindradaField.setColumns(10);

		corField = new JTextField();
		corField.setBounds(104, 171, 86, 20);
		Detalhes.add(corField);
		corField.setColumns(10);

		matriculaField = new JTextField();
		matriculaField.setBounds(104, 133, 86, 20);
		Detalhes.add(matriculaField);
		matriculaField.setColumns(10);

		modeloField = new JTextField();
		modeloField.setBounds(104, 95, 86, 20);
		Detalhes.add(modeloField);
		modeloField.setColumns(10);

		marcaField = new JTextField();
		marcaField.setBounds(104, 54, 86, 20);
		Detalhes.add(marcaField);
		marcaField.setColumns(10);

		JLabel lblAnoDeAquisio = new JLabel("Ano de Aquisi\u00E7\u00E3o");
		lblAnoDeAquisio.setBounds(225, 57, 115, 14);
		Detalhes.add(lblAnoDeAquisio);

		JLabel lblPreoDeAquisio = new JLabel("Pre\u00E7o de Aquisi\u00E7\u00E3o");
		lblPreoDeAquisio.setBounds(225, 98, 115, 14);
		Detalhes.add(lblPreoDeAquisio);

		JLabel lblAnoDeVenda = new JLabel("Ano de Venda");
		lblAnoDeVenda.setBounds(225, 136, 115, 14);
		Detalhes.add(lblAnoDeVenda);

		JLabel lblPreoDeVenda = new JLabel("Pre\u00E7o de Venda");
		lblPreoDeVenda.setBounds(225, 174, 115, 14);
		Detalhes.add(lblPreoDeVenda);

		JLabel lblCombustivel = new JLabel("Combustivel");
		lblCombustivel.setBounds(225, 214, 115, 14);
		Detalhes.add(lblCombustivel);

		aAquisicaoField = new JTextField();
		aAquisicaoField.setBounds(350, 54, 86, 20);
		Detalhes.add(aAquisicaoField);
		aAquisicaoField.setColumns(10);

		pAquisicaoField = new JTextField();
		pAquisicaoField.setBounds(350, 95, 86, 20);
		Detalhes.add(pAquisicaoField);
		pAquisicaoField.setColumns(10);

		aVendaField = new JTextField();
		aVendaField.setBounds(350, 133, 86, 20);
		Detalhes.add(aVendaField);
		aVendaField.setColumns(10);

		pVendaField = new JTextField();
		pVendaField.setBounds(350, 171, 86, 20);
		Detalhes.add(pVendaField);
		pVendaField.setColumns(10);
		try{
			listModelComb = new DefaultComboBoxModel<Combustivel>();
			ResultSet rs = stmt.executeQuery("SELECT * FROM Fuel");
			while(rs.next()){
				Combustivel comb = new Combustivel(
						rs.getInt("idFuel"),
						rs.getString("Fuel"));
				listModelComb.addElement(comb);
			}
		}catch(Exception ee){}
		comboBoxCombustivel = new JComboBox(listModelComb);
		comboBoxCombustivel.setBounds(350, 211, 86, 20);
		Detalhes.add(comboBoxCombustivel);

		btnDetalheMain = new JButton("<");
		btnDetalheMain.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Detalhes.setVisible(false);
				Main.setVisible(true);
			}
		});
		btnDetalheMain.setBounds(21, 260, 89, 23);
		Detalhes.add(btnDetalheMain);

		btnAlterarDetalhes = new JButton("Alterar");
		btnAlterarDetalhes.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if(btnAlterarDetalhes.getText().equals("Alterar")){
					marcaField.setEditable(true);
					modeloField.setEditable(true);
					matriculaField.setEditable(false);
					corField.setEditable(true);
					cilindradaField.setEditable(true);
					aAquisicaoField.setEditable(true);
					pAquisicaoField.setEditable(true);
					aVendaField.setEditable(true);
					pVendaField.setEditable(true);
					comboBoxCombustivel.setEnabled(true);
					btnAlterarDetalhes.setText("Guardar");
				}else
				{
					try{
						Car carSelec = (Car) carList_1.getSelectedValue();
						Class.forName(JDBC_DRIVER);
						conn = DriverManager.getConnection(DB_URL);
						stmt = conn.createStatement();
						if(!matriculaField.getText().isEmpty() && !marcaField.getText().isEmpty() && !modeloField.getText().isEmpty() && !corField.getText().isEmpty() && !cilindradaField.getText().isEmpty() && !aAquisicaoField.getText().isEmpty() && !pAquisicaoField.getText().isEmpty())
						{
							stmt.executeUpdate("UPDATE Cars SET Modelo = '"+modeloField.getText()+"', Cor = '"+
									corField.getText()+"', Marca = '"+marcaField.getText()+"', Matricula = '"+
									matriculaField.getText()+"', Cilindrada = '"+cilindradaField.getText()+"', AnoAquisicao = '"+
									aAquisicaoField.getText()+"', PrecoAquisicao = '"+pAquisicaoField.getText()+"', AnoVenda = '"+
									aVendaField.getText()+"', PrecoVenda = '"+pVendaField.getText()+"', Fuel = '"+
									comboBoxCombustivel.getSelectedItem()+"' WHERE Matricula LIKE '"+carSelec.getMatricula()+"'");
							marcaField.setEditable(false);
							modeloField.setEditable(false);
							matriculaField.setEditable(false);
							corField.setEditable(false);
							cilindradaField.setEditable(false);
							aAquisicaoField.setEditable(false);
							pAquisicaoField.setEditable(false);
							aVendaField.setEditable(false);
							pVendaField.setEditable(false);
							comboBoxCombustivel.setEnabled(false);
							btnAlterarDetalhes.setText("Alterar");
						}else JOptionPane.showMessageDialog(frame, "N�o podem ficar campos em branco excepto o Ano de Venda e Pre�o de Venda");
					}catch(Exception ee){}
					
				}

			}
		});
		btnAlterarDetalhes.setBounds(120, 260, 316, 23);
		Detalhes.add(btnAlterarDetalhes);

		Revisoes = new JPanel();
		frame.getContentPane().add(Revisoes, "name_125374049881135");
		Revisoes.setLayout(null);

		JLabel lblNewLabel_11 = new JLabel("Revisoes");
		lblNewLabel_11.setBounds(60, 24, 66, 14);
		Revisoes.add(lblNewLabel_11);

		revisoesList = new JList(listModelRevisoes);
		revisoesList.setBounds(60, 89, 162, 194);
		Revisoes.add(revisoesList);

		btnDetalhesRev = new JButton("Detalhes");
		btnDetalhesRev.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(revisoesList.getSelectedValue()==null)
				JOptionPane.showMessageDialog(frame, "Selecione uma op��o na lista");
				else{
				Revisoes.setVisible(false);
				DetalhesRevisoes.setVisible(true);
				Revisao revSelec = (Revisao) revisoesList.getSelectedValue();
				idRevField.setText(""+revSelec.getIdRevisao());
				matRevField.setText(revSelec.getMatricula());
				dataRevField.setText(revSelec.getData());
				oficinaRevField.setText(revSelec.getOficina());
				idRevField.setEditable(false);
				matRevField.setEditable(false);
				dataRevField.setEditable(false);
				oficinaRevField.setEditable(false);
				}
			}
		});
		btnDetalhesRev.setBounds(261, 86, 89, 23);
		Revisoes.add(btnDetalhesRev);

		btnAdicionarRev = new JButton("Adicionar");
		btnAdicionarRev.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				Revisoes.setVisible(false);
				AdicionarRevisoes.setVisible(true);

			}
		});
		btnAdicionarRev.setBounds(261, 140, 89, 23);
		Revisoes.add(btnAdicionarRev);

		btnRevisoesMain = new JButton("<");
		btnRevisoesMain.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				Revisoes.setVisible(false);
				Main.setVisible(true);

			}
		});
		btnRevisoesMain.setBounds(60, 288, 89, 23);
		Revisoes.add(btnRevisoesMain);


		frame.getContentPane().add(DetalhesRevisoes, "name_127270865099530");
		DetalhesRevisoes.setLayout(null);

		JLabel lblNewLabel_12 = new JLabel("N\u00BA de Revis\u00E3o");
		lblNewLabel_12.setBounds(24, 76, 80, 14);
		DetalhesRevisoes.add(lblNewLabel_12);

		idRevField = new JTextField();
		idRevField.setBounds(114, 73, 86, 20);
		DetalhesRevisoes.add(idRevField);
		idRevField.setColumns(10);

		JLabel lblNewLabel_13 = new JLabel("Matricula");
		lblNewLabel_13.setBounds(41, 121, 46, 14);
		DetalhesRevisoes.add(lblNewLabel_13);

		matRevField = new JTextField();
		matRevField.setBounds(114, 118, 86, 20);
		DetalhesRevisoes.add(matRevField);
		matRevField.setColumns(10);

		JLabel lblNewLabel_14 = new JLabel("Data");
		lblNewLabel_14.setBounds(41, 164, 46, 14);
		DetalhesRevisoes.add(lblNewLabel_14);

		dataRevField = new JTextField();
		dataRevField.setBounds(114, 161, 86, 20);
		DetalhesRevisoes.add(dataRevField);
		dataRevField.setColumns(10);

		JLabel lblNewLabel_15 = new JLabel("Oficina");
		lblNewLabel_15.setBounds(41, 205, 46, 14);
		DetalhesRevisoes.add(lblNewLabel_15);

		oficinaRevField = new JTextField();
		oficinaRevField.setBounds(114, 202, 86, 20);
		DetalhesRevisoes.add(oficinaRevField);
		oficinaRevField.setColumns(10);

		btnAlterarDetRev = new JButton("Alterar");
		btnAlterarDetRev.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if(btnAlterarDetRev.getText().equals("Alterar")){
					//
					dataRevField.setEditable(true);
					oficinaRevField.setEditable(true);
					btnAlterarDetRev.setText("Guardar");
				}else
				{
					try{
						Revisao revSelec = (Revisao) revisoesList.getSelectedValue();
						Car carSelec = (Car) carList_1.getSelectedValue();
						Class.forName(JDBC_DRIVER);
						conn = DriverManager.getConnection(DB_URL);
						stmt = conn.createStatement();
						stmt.executeUpdate("UPDATE Revisao SET Oficina = '"+oficinaRevField.getText()+"', Data = '"+dataRevField.getText()+"' WHERE IDRevisao = '"+revSelec.getIdRevisao()+"'");
					}catch(Exception ee){}
					dataRevField.setEditable(false);
					oficinaRevField.setEditable(false);
					btnAlterarDetRev.setText("Alterar");
				}

			}
		});
		btnAlterarDetRev.setBounds(313, 121, 89, 23);
		DetalhesRevisoes.add(btnAlterarDetRev);

		JLabel lblNewLabel_16 = new JLabel("Detalhes");
		lblNewLabel_16.setBounds(41, 11, 63, 14);
		DetalhesRevisoes.add(lblNewLabel_16);

		btnDetRevRev = new JButton("<");
		btnDetRevRev.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				DetalhesRevisoes.setVisible(false);
				Revisoes.setVisible(true);

			}
		});
		btnDetRevRev.setBounds(41, 272, 89, 23);
		DetalhesRevisoes.add(btnDetRevRev);


		frame.getContentPane().add(AdicionarRevisoes, "name_127275831845654");
		AdicionarRevisoes.setLayout(null);

		JLabel lblNewLabel_17 = new JLabel("Data");
		lblNewLabel_17.setBounds(59, 88, 46, 14);
		AdicionarRevisoes.add(lblNewLabel_17);

		addDataRevField = new JTextField();
		addDataRevField.setBounds(148, 85, 86, 20);
		AdicionarRevisoes.add(addDataRevField);
		addDataRevField.setColumns(10);

		JLabel lblNewLabel_18 = new JLabel("Oficina");
		lblNewLabel_18.setBounds(59, 135, 46, 14);
		AdicionarRevisoes.add(lblNewLabel_18);

		addOficinaRevField = new JTextField();
		addOficinaRevField.setBounds(148, 132, 86, 20);
		AdicionarRevisoes.add(addOficinaRevField);
		addOficinaRevField.setColumns(10);

		JLabel lblAdicionarNovaReviso = new JLabel("Adicionar nova revis\u00E3o");
		lblAdicionarNovaReviso.setBounds(59, 11, 120, 14);
		AdicionarRevisoes.add(lblAdicionarNovaReviso);

		btnAddRevRev = new JButton("<");
		btnAddRevRev.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				AdicionarRevisoes.setVisible(false);
				Revisoes.setVisible(true);

			}
		});
		btnAddRevRev.setBounds(59, 238, 89, 23);
		AdicionarRevisoes.add(btnAddRevRev);

		btnAddRev = new JButton("Adicionar");
		btnAddRev.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				try{
					Car carSelec = (Car) carList_1.getSelectedValue();
					stmt = conn.createStatement();
					stmt.executeUpdate("INSERT INTO Revisao (Matricula, Oficina, Data) VALUES ('"+carSelec.getMatricula()+"', '"+addOficinaRevField.getText()+"', '"+addDataRevField.getText()+"') ");
				}catch(Exception ee){}
			}
		});
		btnAddRev.setBounds(309, 104, 89, 23);
		AdicionarRevisoes.add(btnAddRev);

		Imposto = new JPanel();
		frame.getContentPane().add(Imposto, "name_57231572586071");
		Imposto.setLayout(null);

		lblNewLabel_19 = new JLabel("Imposto");
		lblNewLabel_19.setBounds(42, 11, 46, 14);
		Imposto.add(lblNewLabel_19);

		impostoList = new JList(listModelImposto);
		impostoList.setBounds(38, 61, 164, 184);
		Imposto.add(impostoList);

		btnDetImposto = new JButton("Detalhes");
		btnDetImposto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(impostoList.getSelectedValue()==null)
					JOptionPane.showMessageDialog(frame, "Selecione uma op��o na lista");
				else{
				Imposto.setVisible(false);
				DetalhesImposto.setVisible(true);
				Imposto impSelec = (Imposto) impostoList.getSelectedValue();
				idImpostoField.setText(""+impSelec.getIdImposto());
				matImpostoField.setText(impSelec.getMatricula());
				dataImpostoField.setText(impSelec.getData());
				valorImpostoField.setText(""+impSelec.getValor());
				idImpostoField.setEditable(false);
				matImpostoField.setEditable(false);
				valorImpostoField.setEditable(false);
				dataImpostoField.setEditable(false);
				}
			}
		});
		btnDetImposto.setBounds(254, 78, 89, 23);
		Imposto.add(btnDetImposto);

		btnAdicionarImposto = new JButton("Adicionar");
		btnAdicionarImposto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				Imposto.setVisible(false);
				AdicionarImposto.setVisible(true);

			}
		});
		btnAdicionarImposto.setBounds(254, 132, 89, 23);
		Imposto.add(btnAdicionarImposto);

		btnImpostoMain = new JButton("<");
		btnImpostoMain.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Imposto.setVisible(false);
				Main.setVisible(true);
			}
		});
		btnImpostoMain.setBounds(42, 275, 89, 23);
		Imposto.add(btnImpostoMain);

		DetalhesImposto = new JPanel();
		frame.getContentPane().add(DetalhesImposto, "name_129286704603391");
		DetalhesImposto.setLayout(null);

		lblNewLabel_20 = new JLabel("Detalhes");
		lblNewLabel_20.setBounds(51, 27, 46, 14);
		DetalhesImposto.add(lblNewLabel_20);

		lblNewLabel_21 = new JLabel("N\u00BA de Imposto");
		lblNewLabel_21.setBounds(20, 86, 77, 14);
		DetalhesImposto.add(lblNewLabel_21);

		lblNewLabel_22 = new JLabel("Matricula");
		lblNewLabel_22.setBounds(51, 143, 46, 14);
		DetalhesImposto.add(lblNewLabel_22);

		lblNewLabel_23 = new JLabel("Data");
		lblNewLabel_23.setBounds(51, 197, 46, 14);
		DetalhesImposto.add(lblNewLabel_23);

		lblNewLabel_24 = new JLabel("Valor");
		lblNewLabel_24.setBounds(51, 251, 46, 14);
		DetalhesImposto.add(lblNewLabel_24);

		idImpostoField = new JTextField();
		idImpostoField.setBounds(126, 83, 86, 20);
		DetalhesImposto.add(idImpostoField);
		idImpostoField.setColumns(10);

		matImpostoField = new JTextField();
		matImpostoField.setBounds(126, 140, 86, 20);
		DetalhesImposto.add(matImpostoField);
		matImpostoField.setColumns(10);

		dataImpostoField = new JTextField();
		dataImpostoField.setBounds(126, 194, 86, 20);
		DetalhesImposto.add(dataImpostoField);
		dataImpostoField.setColumns(10);

		valorImpostoField = new JTextField();
		valorImpostoField.setBounds(126, 248, 86, 20);
		DetalhesImposto.add(valorImpostoField);
		valorImpostoField.setColumns(10);

		btnAlterarImposto = new JButton("Alterar");
		btnAlterarImposto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Imposto impSelec = (Imposto) impostoList.getSelectedValue();
				if(btnAlterarImposto.getText().equals("Alterar")){
					//
					dataImpostoField.setEditable(true);
					valorImpostoField.setEditable(true);
					btnAlterarImposto.setText("Guardar");
				}else
				{
					try{
						Class.forName(JDBC_DRIVER);
						conn = DriverManager.getConnection(DB_URL);
						stmt = conn.createStatement();
						stmt.executeUpdate("UPDATE Imposto SET Valor = '"+valorImpostoField.getText()+"', Data = '"+dataImpostoField.getText()+"' WHERE IDImposto = '"+impSelec.getIdImposto()+"'");
					}catch(Exception ee){}
					dataImpostoField.setEditable(false);
					valorImpostoField.setEditable(false);
					btnAlterarImposto.setText("Alterar");
				}

			}
		});
		btnAlterarImposto.setBounds(318, 139, 89, 23);
		DetalhesImposto.add(btnAlterarImposto);

		btnDetImpostoImposto = new JButton("<");
		btnDetImpostoImposto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				DetalhesImposto.setVisible(false);
				Imposto.setVisible(true);
			}
		});
		btnDetImpostoImposto.setBounds(51, 288, 89, 23);
		DetalhesImposto.add(btnDetImpostoImposto);

		AdicionarImposto = new JPanel();
		frame.getContentPane().add(AdicionarImposto, "name_129290901576159");
		AdicionarImposto.setLayout(null);

		lblNewLabel_25 = new JLabel("Adicionar novo Imposto");
		lblNewLabel_25.setBounds(52, 26, 165, 14);
		AdicionarImposto.add(lblNewLabel_25);

		lblNewLabel_26 = new JLabel("Data");
		lblNewLabel_26.setBounds(52, 95, 46, 14);
		AdicionarImposto.add(lblNewLabel_26);

		lblNewLabel_27 = new JLabel("Valor");
		lblNewLabel_27.setBounds(52, 138, 46, 14);
		AdicionarImposto.add(lblNewLabel_27);

		addDataImpostoField = new JTextField();
		addDataImpostoField.setBounds(131, 92, 86, 20);
		AdicionarImposto.add(addDataImpostoField);
		addDataImpostoField.setColumns(10);

		addValorImpostoField = new JTextField();
		addValorImpostoField.setBounds(131, 135, 86, 20);
		AdicionarImposto.add(addValorImpostoField);
		addValorImpostoField.setColumns(10);

		btnAddImposto = new JButton("Adicionar");
		btnAddImposto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				try{
					Car carSelec = (Car) carList_1.getSelectedValue();
					stmt = conn.createStatement();
					stmt.executeUpdate("INSERT INTO Imposto (Matricula, Valor, Data) VALUES ('"+carSelec.getMatricula()+"', '"+addValorImpostoField.getText()+"', '"+addDataImpostoField.getText()+"') ");
				}catch(Exception ee){}

			}
		});
		btnAddImposto.setBounds(300, 105, 89, 23);
		AdicionarImposto.add(btnAddImposto);

		btwAddImpostoImposto = new JButton("<");
		btwAddImpostoImposto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				AdicionarImposto.setVisible(false);
				Imposto.setVisible(true);
			}
		});
		btwAddImpostoImposto.setBounds(52, 234, 89, 23);
		AdicionarImposto.add(btwAddImpostoImposto);

		Inspecao = new JPanel();
		frame.getContentPane().add(Inspecao, "name_125255944500858");
		Inspecao.setLayout(null);

		inspecaoList = new JList(listModelInspecao);
		inspecaoList.setBounds(51, 76, 132, 164);
		Inspecao.add(inspecaoList);

		btnDetInspecao = new JButton("Detalhes");
		btnDetInspecao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(inspecaoList.getSelectedValue()==null)
					JOptionPane.showMessageDialog(frame, "Selecione uma op��o na lista");
				else{
				Inspecao.setVisible(false);
				DetalhesInspecao.setVisible(true);
				Inspecao inspSelec = (Inspecao) inspecaoList.getSelectedValue();
				idInspecaoField.setText(""+inspSelec.getIdInspecao());
				matInspecaoField.setText(inspSelec.getMatricula());
				dataInspecaoField.setText(inspSelec.getData());
				valorInspecaoField.setText(""+inspSelec.getValor());
				idInspecaoField.setEditable(false);
				matInspecaoField.setEditable(false);
				valorInspecaoField.setEditable(false);
				dataInspecaoField.setEditable(false);	
				}
			}
		});
		btnDetInspecao.setBounds(267, 73, 89, 23);
		Inspecao.add(btnDetInspecao);

		btnAdicionarInspecao = new JButton("Adicionar");
		btnAdicionarInspecao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				Inspecao.setVisible(false);
				AdicionarInspecao.setVisible(true);

			}
		});
		btnAdicionarInspecao.setBounds(267, 127, 89, 23);
		Inspecao.add(btnAdicionarInspecao);

		btnInspecaoMain = new JButton("<");
		btnInspecaoMain.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				Inspecao.setVisible(false);
				Main.setVisible(true);
			}
		});
		btnInspecaoMain.setBounds(55, 270, 89, 23);
		Inspecao.add(btnInspecaoMain);

		lblNewLabel_34 = new JLabel("Inspe\u00E7\u00E3o");
		lblNewLabel_34.setBounds(51, 22, 93, 14);
		Inspecao.add(lblNewLabel_34);

		DetalhesInspecao = new JPanel();
		frame.getContentPane().add(DetalhesInspecao, "name_129381748173089");
		DetalhesInspecao.setLayout(null);

		lblDetalhes = new JLabel("Detalhes");
		lblDetalhes.setBounds(56, 27, 46, 14);
		DetalhesInspecao.add(lblDetalhes);

		label_1 = new JLabel("N\u00BA de Imposto");
		label_1.setBounds(25, 86, 77, 14);
		DetalhesInspecao.add(label_1);

		idInspecaoField = new JTextField();
		idInspecaoField.setColumns(10);
		idInspecaoField.setBounds(131, 83, 86, 20);
		DetalhesInspecao.add(idInspecaoField);

		btnAlterarInspecao = new JButton("Alterar");
		btnAlterarInspecao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				Inspecao inspSelec = (Inspecao) inspecaoList.getSelectedValue();
				if(btnAlterarInspecao.getText().equals("Alterar")){
					//
					dataInspecaoField.setEditable(true);
					valorInspecaoField.setEditable(true);
					btnAlterarInspecao.setText("Guardar");
				}else
				{
					try{
						Class.forName(JDBC_DRIVER);
						conn = DriverManager.getConnection(DB_URL);
						stmt = conn.createStatement();
						stmt.executeUpdate("UPDATE Inspecao SET Valor = '"+valorInspecaoField.getText()+"', Data = '"+dataInspecaoField.getText()+"' WHERE IDInspecao = '"+inspSelec.getIdInspecao()+"'");
					}catch(Exception ee){}
					dataInspecaoField.setEditable(false);
					valorInspecaoField.setEditable(false);
					btnAlterarInspecao.setText("Alterar");
				}

			}
		});
		btnAlterarInspecao.setBounds(323, 139, 89, 23);
		DetalhesInspecao.add(btnAlterarInspecao);

		matInspecaoField = new JTextField();
		matInspecaoField.setColumns(10);
		matInspecaoField.setBounds(131, 140, 86, 20);
		DetalhesInspecao.add(matInspecaoField);

		label_2 = new JLabel("Matricula");
		label_2.setBounds(56, 143, 46, 14);
		DetalhesInspecao.add(label_2);

		label_3 = new JLabel("Data");
		label_3.setBounds(56, 197, 46, 14);
		DetalhesInspecao.add(label_3);

		dataInspecaoField = new JTextField();
		dataInspecaoField.setColumns(10);
		dataInspecaoField.setBounds(131, 194, 86, 20);
		DetalhesInspecao.add(dataInspecaoField);

		valorInspecaoField = new JTextField();
		valorInspecaoField.setColumns(10);
		valorInspecaoField.setBounds(131, 248, 86, 20);
		DetalhesInspecao.add(valorInspecaoField);

		label_4 = new JLabel("Valor");
		label_4.setBounds(56, 251, 46, 14);
		DetalhesInspecao.add(label_4);

		btnDetInspecaoInspecao = new JButton("<");
		btnDetInspecaoInspecao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				DetalhesInspecao.setVisible(false);
				Inspecao.setVisible(true);

			}
		});
		btnDetInspecaoInspecao.setBounds(56, 288, 89, 23);
		DetalhesInspecao.add(btnDetInspecaoInspecao);

		AdicionarInspecao = new JPanel();
		frame.getContentPane().add(AdicionarInspecao, "name_129385316493319");
		AdicionarInspecao.setLayout(null);

		lblAdicionarNovaInspeo = new JLabel("Adicionar nova Inspe\u00E7\u00E3o");
		lblAdicionarNovaInspeo.setBounds(27, 55, 195, 14);
		AdicionarInspecao.add(lblAdicionarNovaInspeo);

		lblData_1 = new JLabel("Data");
		lblData_1.setBounds(27, 124, 46, 14);
		AdicionarInspecao.add(lblData_1);

		addDataInspecaoField = new JTextField();
		addDataInspecaoField.setColumns(10);
		addDataInspecaoField.setBounds(106, 121, 86, 20);
		AdicionarInspecao.add(addDataInspecaoField);

		btwAddInspecao = new JButton("Adicionar");
		btwAddInspecao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				try{
					Car carSelec = (Car) carList_1.getSelectedValue();
					stmt = conn.createStatement();
					stmt.executeUpdate("INSERT INTO Inspecao (Matricula, Valor, Data) VALUES ('"+carSelec.getMatricula()+"', '"+addValorInspecaoField.getText()+"', '"+addDataInspecaoField.getText()+"') ");
				}catch(Exception ee){}

			}
		});
		btwAddInspecao.setBounds(275, 134, 89, 23);
		AdicionarInspecao.add(btwAddInspecao);

		addValorInspecaoField = new JTextField();
		addValorInspecaoField.setColumns(10);
		addValorInspecaoField.setBounds(106, 164, 86, 20);
		AdicionarInspecao.add(addValorInspecaoField);

		lblValor_1 = new JLabel("Valor");
		lblValor_1.setBounds(27, 167, 46, 14);
		AdicionarInspecao.add(lblValor_1);

		btwAddInspecaoInspecao = new JButton("<");
		btwAddInspecaoInspecao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				AdicionarInspecao.setVisible(false);
				Inspecao.setVisible(true);

			}
		});
		btwAddInspecaoInspecao.setBounds(27, 263, 89, 23);
		AdicionarInspecao.add(btwAddInspecaoInspecao);

		Abastecimento = new JPanel();
		frame.getContentPane().add(Abastecimento, "name_125290853212424");
		Abastecimento.setLayout(null);

		abastecimentoList = new JList(listModelAbastecimento);
		abastecimentoList.setBounds(33, 66, 148, 164);
		Abastecimento.add(abastecimentoList);

		btnDetAbastecimento = new JButton("Detalhes");
		btnDetAbastecimento.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(abastecimentoList.getSelectedValue()==null)
					JOptionPane.showMessageDialog(frame, "Selecione uma op��o na lista");
				else{
				Abastecimento.setVisible(false);
				DetalhesAbastecimento.setVisible(true);
				Abastecimento abastSelec = (Abastecimento) abastecimentoList.getSelectedValue();
				idAbastField.setText(""+abastSelec.getIdAbastecimento());
				matAbastField.setText(abastSelec.getMatricula());
				localAbastField.setText(abastSelec.getLocal());
				kmsAbastField.setText(""+abastSelec.getKMs());
				litrosAbastField.setText(""+abastSelec.getLitros());
				dataAbastField.setText(abastSelec.getData());
				valorAbastField.setText(""+abastSelec.getValor());
				idAbastField.setEditable(false);
				matAbastField.setEditable(false);
				localAbastField.setEditable(false);
				kmsAbastField.setEditable(false);
				litrosAbastField.setEditable(false);
				valorAbastField.setEditable(false);
				dataAbastField.setEditable(false);
				}
			}
		});
		btnDetAbastecimento.setBounds(249, 63, 89, 23);
		Abastecimento.add(btnDetAbastecimento);

		btnAdicionarAbastecimento = new JButton("Adicionar");
		btnAdicionarAbastecimento.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				Abastecimento.setVisible(false);
				AdicionarAbastecimento.setVisible(true);

			}
		});
		btnAdicionarAbastecimento.setBounds(249, 117, 89, 23);
		Abastecimento.add(btnAdicionarAbastecimento);

		btnAbastecimentoMain = new JButton("<");
		btnAbastecimentoMain.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				Abastecimento.setVisible(false);
				Main.setVisible(true);

			}
		});
		btnAbastecimentoMain.setBounds(37, 260, 89, 23);
		Abastecimento.add(btnAbastecimentoMain);

		lblAbastecimentos = new JLabel("Abastecimentos");
		lblAbastecimentos.setBounds(33, 26, 93, 14);
		Abastecimento.add(lblAbastecimentos);

		DetalhesAbastecimento = new JPanel();
		frame.getContentPane().add(DetalhesAbastecimento, "name_129429567401643");
		DetalhesAbastecimento.setLayout(null);

		lblNewLabel_28 = new JLabel("Detalhes");
		lblNewLabel_28.setBounds(43, 27, 67, 14);
		DetalhesAbastecimento.add(lblNewLabel_28);

		lblNewLabel_29 = new JLabel("N\u00BA");
		lblNewLabel_29.setBounds(43, 82, 46, 14);
		DetalhesAbastecimento.add(lblNewLabel_29);

		lblNewLabel_30 = new JLabel("Matricula");
		lblNewLabel_30.setBounds(43, 130, 46, 14);
		DetalhesAbastecimento.add(lblNewLabel_30);

		lblNewLabel_31 = new JLabel("Local");
		lblNewLabel_31.setBounds(43, 182, 46, 14);
		DetalhesAbastecimento.add(lblNewLabel_31);

		lblNewLabel_32 = new JLabel("Kms");
		lblNewLabel_32.setBounds(43, 235, 46, 14);
		DetalhesAbastecimento.add(lblNewLabel_32);

		idAbastField = new JTextField();
		idAbastField.setBounds(128, 79, 86, 20);
		DetalhesAbastecimento.add(idAbastField);
		idAbastField.setColumns(10);

		matAbastField = new JTextField();
		matAbastField.setBounds(128, 127, 86, 20);
		DetalhesAbastecimento.add(matAbastField);
		matAbastField.setColumns(10);

		localAbastField = new JTextField();
		localAbastField.setBounds(128, 179, 86, 20);
		DetalhesAbastecimento.add(localAbastField);
		localAbastField.setColumns(10);

		kmsAbastField = new JTextField();
		kmsAbastField.setBounds(128, 232, 86, 20);
		DetalhesAbastecimento.add(kmsAbastField);
		kmsAbastField.setColumns(10);

		lblLitros = new JLabel("Litros");
		lblLitros.setBounds(262, 79, 46, 14);
		DetalhesAbastecimento.add(lblLitros);

		litrosAbastField = new JTextField();
		litrosAbastField.setColumns(10);
		litrosAbastField.setBounds(347, 76, 86, 20);
		DetalhesAbastecimento.add(litrosAbastField);

		dataAbastField = new JTextField();
		dataAbastField.setColumns(10);
		dataAbastField.setBounds(347, 124, 86, 20);
		DetalhesAbastecimento.add(dataAbastField);

		lblData = new JLabel("Data");
		lblData.setBounds(262, 127, 46, 14);
		DetalhesAbastecimento.add(lblData);

		lblValor = new JLabel("Valor");
		lblValor.setBounds(262, 179, 46, 14);
		DetalhesAbastecimento.add(lblValor);

		valorAbastField = new JTextField();
		valorAbastField.setColumns(10);
		valorAbastField.setBounds(347, 176, 86, 20);
		DetalhesAbastecimento.add(valorAbastField);

		btnDetAbastAbast = new JButton("<");
		btnDetAbastAbast.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				DetalhesAbastecimento.setVisible(false);
				Abastecimento.setVisible(true);
			}
		});
		btnDetAbastAbast.setBounds(43, 288, 89, 23);
		DetalhesAbastecimento.add(btnDetAbastAbast);

		btnAlterarAbast = new JButton("Alterar");
		btnAlterarAbast.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Abastecimento abastSelec = (Abastecimento) abastecimentoList.getSelectedValue();
				if(btnAlterarAbast.getText().equals("Alterar")){
					//
					localAbastField.setEditable(true);
					kmsAbastField.setEditable(true);
					litrosAbastField.setEditable(true);
					dataAbastField.setEditable(true);
					valorAbastField.setEditable(true);
					btnAlterarAbast.setText("Guardar");
				}else
				{
					try{
						Class.forName(JDBC_DRIVER);
						conn = DriverManager.getConnection(DB_URL);
						stmt = conn.createStatement();
						stmt.executeUpdate("UPDATE Abastecimento SET Valor = '"+valorAbastField.getText()+"', Local = '"+localAbastField.getText()+"', KMs = '"+kmsAbastField.getText()+"', Litros = '"+litrosAbastField.getText()+"', Data = '"+dataAbastField.getText()+"' WHERE IDAbastecimento = '"+abastSelec.getIdAbastecimento()+"'");
					}catch(Exception ee){}
					localAbastField.setEditable(false);
					kmsAbastField.setEditable(false);
					litrosAbastField.setEditable(false);
					dataAbastField.setEditable(false);
					valorAbastField.setEditable(false);
					btnAlterarAbast.setText("Alterar");
				}
			}
		});
		btnAlterarAbast.setBounds(234, 288, 89, 23);
		DetalhesAbastecimento.add(btnAlterarAbast);

		AdicionarAbastecimento = new JPanel();
		frame.getContentPane().add(AdicionarAbastecimento, "name_129432925306110");
		AdicionarAbastecimento.setLayout(null);

		label_8 = new JLabel("Local");
		label_8.setBounds(36, 88, 46, 14);
		AdicionarAbastecimento.add(label_8);

		addLocalAbastField = new JTextField();
		addLocalAbastField.setColumns(10);
		addLocalAbastField.setBounds(92, 85, 86, 20);
		AdicionarAbastecimento.add(addLocalAbastField);

		label_9 = new JLabel("Kms");
		label_9.setBounds(36, 119, 46, 14);
		AdicionarAbastecimento.add(label_9);

		addKmsAbastField = new JTextField();
		addKmsAbastField.setColumns(10);
		addKmsAbastField.setBounds(92, 116, 86, 20);
		AdicionarAbastecimento.add(addKmsAbastField);

		label_10 = new JLabel("Litros");
		label_10.setBounds(36, 153, 46, 14);
		AdicionarAbastecimento.add(label_10);

		addLitrosAbastField = new JTextField();
		addLitrosAbastField.setColumns(10);
		addLitrosAbastField.setBounds(92, 150, 86, 20);
		AdicionarAbastecimento.add(addLitrosAbastField);

		addDataAbastField = new JTextField();
		addDataAbastField.setColumns(10);
		addDataAbastField.setBounds(92, 185, 86, 20);
		AdicionarAbastecimento.add(addDataAbastField);

		label_11 = new JLabel("Data");
		label_11.setBounds(36, 188, 46, 14);
		AdicionarAbastecimento.add(label_11);

		label_12 = new JLabel("Valor");
		label_12.setBounds(36, 219, 46, 14);
		AdicionarAbastecimento.add(label_12);

		addValorAbastField = new JTextField();
		addValorAbastField.setColumns(10);
		addValorAbastField.setBounds(92, 216, 86, 20);
		AdicionarAbastecimento.add(addValorAbastField);

		lblNewLabel_33 = new JLabel("Adicionar novo abastecimento");
		lblNewLabel_33.setBounds(36, 27, 167, 14);
		AdicionarAbastecimento.add(lblNewLabel_33);

		btnAddAbast = new JButton("Adicionar");
		btnAddAbast.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				try{
					Car carSelec = (Car) carList_1.getSelectedValue();
					stmt = conn.createStatement();
					stmt.executeUpdate("INSERT INTO Abastecimento (Matricula, Valor, Data, KMs, Litros, Local) VALUES ('"+carSelec.getMatricula()+"', '"+addValorAbastField.getText()+"', '"+addDataAbastField.getText()+"', '"+addKmsAbastField.getText()+"', '"+addLitrosAbastField.getText()+"', '"+addLocalAbastField.getText()+"') ");
				}catch(Exception ee){}

			}
		});
		btnAddAbast.setBounds(289, 149, 89, 23);
		AdicionarAbastecimento.add(btnAddAbast);

		btnAddAbastAbast = new JButton("<");
		btnAddAbastAbast.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				AdicionarAbastecimento.setVisible(false);
				Abastecimento.setVisible(true);

			}
		});
		btnAddAbastAbast.setBounds(36, 272, 89, 23);
		AdicionarAbastecimento.add(btnAddAbastAbast);

		Adicionar = new JPanel();
		frame.getContentPane().add(Adicionar, "name_57229718358959");
		Adicionar.setLayout(null);

		JLabel lblNewLabel = new JLabel("Adicionar novo carro");
		lblNewLabel.setBounds(45, 11, 126, 14);
		Adicionar.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Marca");
		lblNewLabel_1.setBounds(45, 57, 46, 14);
		Adicionar.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Modelo");
		lblNewLabel_2.setBounds(45, 97, 46, 14);
		Adicionar.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Matricula");
		lblNewLabel_3.setBounds(45, 140, 46, 14);
		Adicionar.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("Cor");
		lblNewLabel_4.setBounds(45, 179, 46, 14);
		Adicionar.add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel("Cilindrada");
		lblNewLabel_5.setBounds(45, 223, 52, 14);
		Adicionar.add(lblNewLabel_5);

		addMarcaField = new JTextField();
		addMarcaField.setBounds(101, 54, 86, 20);
		Adicionar.add(addMarcaField);
		addMarcaField.setColumns(10);

		addModeloField = new JTextField();
		addModeloField.setBounds(101, 94, 86, 20);
		Adicionar.add(addModeloField);
		addModeloField.setColumns(10);

		addMatField = new JTextField();
		addMatField.setBounds(101, 137, 86, 20);
		Adicionar.add(addMatField);
		addMatField.setColumns(10);

		addCorField = new JTextField();
		addCorField.setBounds(101, 176, 86, 20);
		Adicionar.add(addCorField);
		addCorField.setColumns(10);

		addCilindradaField = new JTextField();
		addCilindradaField.setBounds(101, 220, 86, 20);
		Adicionar.add(addCilindradaField);
		addCilindradaField.setColumns(10);

		JLabel lblNewLabel_6 = new JLabel("Ano de Aquisi\u00E7\u00E3o");
		lblNewLabel_6.setBounds(197, 57, 90, 14);
		Adicionar.add(lblNewLabel_6);

		JLabel lblNewLabel_7 = new JLabel("Pre\u00E7o de Aquisi\u00E7\u00E3o");
		lblNewLabel_7.setBounds(197, 97, 90, 14);
		Adicionar.add(lblNewLabel_7);

		JLabel lblNewLabel_8 = new JLabel("Ano de Venda");
		lblNewLabel_8.setBounds(197, 140, 90, 14);
		Adicionar.add(lblNewLabel_8);

		JLabel lblNewLabel_9 = new JLabel("Pre\u00E7o de Venda");
		lblNewLabel_9.setBounds(197, 179, 90, 14);
		Adicionar.add(lblNewLabel_9);

		JLabel lblNewLabel_10 = new JLabel("Combustivel");
		lblNewLabel_10.setBounds(197, 223, 90, 14);
		Adicionar.add(lblNewLabel_10);

		addAnoAquiField = new JTextField();
		addAnoAquiField.setBounds(297, 54, 86, 20);
		Adicionar.add(addAnoAquiField);
		addAnoAquiField.setColumns(10);

		addPrecoAquiField = new JTextField();
		addPrecoAquiField.setBounds(297, 94, 86, 20);
		Adicionar.add(addPrecoAquiField);
		addPrecoAquiField.setColumns(10);

		addAnoVendaField = new JTextField();
		addAnoVendaField.setBounds(297, 137, 86, 20);
		Adicionar.add(addAnoVendaField);
		addAnoVendaField.setColumns(10);

		addPrecoVendaField = new JTextField();
		addPrecoVendaField.setBounds(297, 176, 86, 20);
		Adicionar.add(addPrecoVendaField);
		addPrecoVendaField.setColumns(10);

		comboBoxAddCarroComb = new JComboBox(listModelComb);
		comboBoxAddCarroComb.setBounds(297, 220, 86, 20);
		Adicionar.add(comboBoxAddCarroComb);

		btnAdicionarMain = new JButton("<");
		btnAdicionarMain.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Adicionar.setVisible(false);
				Main.setVisible(true);
			}
		});
		btnAdicionarMain.setBounds(45, 288, 89, 23);
		Adicionar.add(btnAdicionarMain);

		btnAdicionarCarro = new JButton("Adicionar");
		btnAdicionarCarro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try{
					stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery("SELECT * FROM Cars");
					int veriMtr=0;
					while(rs.next()){
						String mtr = rs.getString(1);
						if(addMatField.getText().equals(mtr) )
							veriMtr++;
					}
					if(!addMatField.getText().isEmpty() && !addMarcaField.getText().isEmpty() && !addModeloField.getText().isEmpty() && !addCorField.getText().isEmpty() && !addCilindradaField.getText().isEmpty() && !addAnoAquiField.getText().isEmpty() && !addPrecoAquiField.getText().isEmpty() && veriMtr==0)
						stmt.executeUpdate("INSERT INTO Cars VALUES ('"+addMatField.getText()+"', '"+addMarcaField.getText()+"', '"+addModeloField.getText()+"', '"+addCorField.getText()+"', '"+addCilindradaField.getText()+"', '"+addAnoAquiField.getText()+"', '"+addPrecoAquiField.getText()+"', '"+addAnoVendaField.getText()+"', '"+addPrecoVendaField.getText()+"', '"+comboBoxAddCarroComb.getSelectedItem()+"', '"+saveUser+"')");
					else if(veriMtr==0)
						JOptionPane.showMessageDialog(frame, "Obrigat�rio preencher todos os campos excepto Ano de Venda e Pre�o de venda");
					else
						JOptionPane.showMessageDialog(frame, "A matricula que tentou inserir j� existe");
				}catch(Exception ee){}
			}
		});
		btnAdicionarCarro.setBounds(197, 288, 89, 23);
		Adicionar.add(btnAdicionarCarro);
	}

	public JTextField getUserField() {
		return userField;
	}
	public JPasswordField getPassField() {
		return passField;
	}
	public JButton getBtnLogin() {
		return btnLogin;
	}
	public JButton getBtnRegister() {
		return btnRegister;
	}
	public JButton getBtnAnonymous() {
		return btnAnonymous;
	}
	public JList getList() {
		return list;
	}
	public JList getCarList() {
		return carList_1;
	}
	private void fillList(String sql){
		listModel.clear();
		try{
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()){
				Car cl = new Car(
						rs.getString("Matricula"),
						rs.getString("Marca"),
						rs.getString("Modelo"),
						rs.getString("Cor"),
						rs.getString("Fuel"),
						rs.getString("Username"),
						rs.getInt("Cilindrada"),
						rs.getDouble("PrecoAquisicao"),
						rs.getDouble("PrecoVenda"),
						rs.getInt("AnoAquisicao"),
						rs.getInt("AnoVenda"));
				listModel.addElement(cl);
			}
		} catch(Exception ee){
			System.out.println(ee.getMessage()+"Erro a preencher a lista");
		}}
	public JButton getBtnRegister_1() {
		return btnRegister_1;
	}
	public JButton getBtnDetalhes() {
		return btnDetalhes;
	}
	public JButton getBtnDetalheMain() {
		return btnDetalheMain;
	}
	public JButton getBtnAlterarDetalhes() {
		return btnAlterarDetalhes;
	}
	public JButton getBtnBackRegister() {
		return btnBackRegister;
	}
	public JComboBox getComboBoxCombustivel() {
		return comboBoxCombustivel;
	}
	public JButton getBtnRevisoes() {
		return btnRevisoes;
	}
	public JList getRevisoesList() {
		return revisoesList;
	}
	public JButton getBtnRevisoesMain() {
		return btnRevisoesMain;
	}
	public JButton getBtnDetalhesRev() {
		return btnDetalhesRev;
	}
	public JButton getBtnDetRevRev() {
		return btnDetRevRev;
	}
	public JButton getBtnAlterarDetRev() {
		return btnAlterarDetRev;
	}
	public JButton getBtnAdicionarRev() {
		return btnAdicionarRev;
	}
	public JButton getBtnAddRevRev() {
		return btnAddRevRev;
	}
	public JButton getBtnAddRev() {
		return btnAddRev;
	}
	public JButton getBtnImposto() {
		return btnImposto;
	}
	public JList getImpostoList() {
		return impostoList;
	}
	public JButton getBtnDetImposto() {
		return btnDetImposto;
	}
	public JButton getBtnDetImpostoImposto() {
		return btnDetImpostoImposto;
	}
	public JButton getBtnAlterarImposto() {
		return btnAlterarImposto;
	}
	public JButton getBtwAddImpostoImposto() {
		return btwAddImpostoImposto;
	}
	public JButton getBtnAddImposto() {
		return btnAddImposto;
	}
	public JButton getBtnAdicionarImposto() {
		return btnAdicionarImposto;
	}
	public JButton getBtnImpostoMain() {
		return btnImpostoMain;
	}
	public JButton getBtnInspecao() {
		return btnInspecao;
	}
	public JList getInspecaoList() {
		return inspecaoList;
	}
	public JButton getBtnInspecaoMain() {
		return btnInspecaoMain;
	}
	public JButton getBtnDetInspecao() {
		return btnDetInspecao;
	}
	public JButton getBtnDetInspecaoInspecao() {
		return btnDetInspecaoInspecao;
	}
	public JButton getBtnAlterarInspecao() {
		return btnAlterarInspecao;
	}
	public JButton getBtnAdicionarInspecao() {
		return btnAdicionarInspecao;
	}
	public JButton getBtwAddInspecaoInspecao() {
		return btwAddInspecaoInspecao;
	}
	public JButton getBtwAddInspecao() {
		return btwAddInspecao;
	}
	public JButton getBtnAbastecimento() {
		return btnAbastecimento;
	}
	public JButton getBtnAbastecimentoMain() {
		return btnAbastecimentoMain;
	}
	public JList getAbastecimentoList() {
		return abastecimentoList;
	}
	public JButton getBtnDetAbastecimento() {
		return btnDetAbastecimento;
	}
	public JButton getBtnDetAbastAbast() {
		return btnDetAbastAbast;
	}
	public JButton getBtnAlterarAbast() {
		return btnAlterarAbast;
	}
	public JButton getBtnAdicionarAbastecimento() {
		return btnAdicionarAbastecimento;
	}
	public JButton getBtnAddAbastAbast() {
		return btnAddAbastAbast;
	}
	public JButton getBtnAddAbast() {
		return btnAddAbast;
	}
	public JButton getBtnAdicionar() {
		return btnAdicionar;
	}
	public JButton getBtnAdicionarMain() {
		return btnAdicionarMain;
	}
	public JComboBox getComboBoxAddCarroComb() {
		return comboBoxAddCarroComb;
	}
	public JButton getBtnAdicionarCarro() {
		return btnAdicionarCarro;
	}
}
