import java.sql.Date;

public class Inspecao {

	int idInspecao;
	String matricula;
	double valor;
	String data;
	
	public String toString() {
		return idInspecao+" - "+data;
	}
	/**
	 * @param idInspecao
	 * @param matricula
	 * @param valor
	 * @param data
	 */
	public Inspecao(int idInspecao, String matricula, double valor, String data) {
		super();
		this.idInspecao = idInspecao;
		this.matricula = matricula;
		this.valor = valor;
		this.data = data;
	}
	/**
	 * @return the idInspecao
	 */
	public int getIdInspecao() {
		return idInspecao;
	}
	/**
	 * @param idInspecao the idInspecao to set
	 */
	public void setIdInspecao(int idInspecao) {
		this.idInspecao = idInspecao;
	}
	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return matricula;
	}
	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	/**
	 * @return the valor
	 */
	public double getValor() {
		return valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(double valor) {
		this.valor = valor;
	}
	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}
	
	
	
	
}
