import java.sql.Date;

public class Imposto {

	int idImposto;
	String matricula;
	double valor;
	String data;
	
	public String toString() {
		return idImposto+" - "+data;
	}
	
	/**
	 * @param idImposto
	 * @param matricula
	 * @param valor
	 * @param data
	 */
	public Imposto(int idImposto, String matricula, double valor, String data) {
		super();
		this.idImposto = idImposto;
		this.matricula = matricula;
		this.valor = valor;
		this.data = data;
	}
	
	
	/**
	 * @return the idImposto
	 */
	public int getIdImposto() {
		return idImposto;
	}
	/**
	 * @param idImposto the idImposto to set
	 */
	public void setIdImposto(int idImposto) {
		this.idImposto = idImposto;
	}
	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return matricula;
	}
	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	/**
	 * @return the valor
	 */
	public double getValor() {
		return valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(double valor) {
		this.valor = valor;
	}
	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}
	
	
	
}
