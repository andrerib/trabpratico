import java.sql.Date;

public class Revisao {

	
	public String toString() {
		return idRevisao+" - "+data;
	}
	int idRevisao;
	String matricula, oficina;
	String data;
	
	/**
	 * @param idRevisao
	 * @param matricula
	 * @param oficina
	 * @param data
	 */
	
	public Revisao(int idRevisao, String matricula, String oficina, String data) {
		super();
		this.idRevisao = idRevisao;
		this.matricula = matricula;
		this.oficina = oficina;
		this.data = data;
	}
	/**
	 * @return the idRevisao
	 */
	public int getIdRevisao() {
		return idRevisao;
	}
	/**
	 * @param idRevisao the idRevisao to set
	 */
	public void setIdRevisao(int idRevisao) {
		this.idRevisao = idRevisao;
	}
	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return matricula;
	}
	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	/**
	 * @return the oficina
	 */
	public String getOficina() {
		return oficina;
	}
	/**
	 * @param oficina the oficina to set
	 */
	public void setOficina(String oficina) {
		this.oficina = oficina;
	}
	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}
	
	
	
}
