import java.sql.Date;

public class Abastecimento {

	int idAbastecimento;
	String matricula, local;
	double KMs, litros, valor;
	String data;
	
	
	/**
	 * @param idAbastecimento
	 * @param matricula
	 * @param local
	 * @param kMs
	 * @param litros
	 * @param valor
	 * @param data
	 */
	
	
	
	public Abastecimento(int idAbastecimento, String matricula, String local, double kMs, double litros,
			double valor, String data) {
		super();
		this.idAbastecimento = idAbastecimento;
		this.matricula = matricula;
		this.local = local;
		this.KMs = kMs;
		this.litros = litros;
		this.valor = valor;
		this.data = data;
	}
	

	@Override
	public String toString() {
		return idAbastecimento+" - " +data;
	}


	/**
	 * @return the idAbastecimento
	 */
	public int getIdAbastecimento() {
		return idAbastecimento;
	}
	/**
	 * @param idAbastecimento the idAbastecimento to set
	 */
	public void setIdAbastecimento(int idAbastecimento) {
		this.idAbastecimento = idAbastecimento;
	}
	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return matricula;
	}
	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	/**
	 * @return the local
	 */
	public String getLocal() {
		return local;
	}
	/**
	 * @param local the local to set
	 */
	public void setLocal(String local) {
		this.local = local;
	}
	/**
	 * @return the kMs
	 */
	public double getKMs() {
		return KMs;
	}
	/**
	 * @param kMs the kMs to set
	 */
	public void setKMs(double kMs) {
		KMs = kMs;
	}
	/**
	 * @return the litros
	 */
	public double getLitros() {
		return litros;
	}
	/**
	 * @param litros the litros to set
	 */
	public void setLitros(double litros) {
		this.litros = litros;
	}
	/**
	 * @return the valor
	 */
	public double getValor() {
		return valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(double valor) {
		this.valor = valor;
	}
	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}
	
	
	
	
}
