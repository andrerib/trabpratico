import java.sql.Date;

public class Car {

	String matricula, marca, modelo, cor, fuel, username;
	int cilindrada;
	double precoAquisicao, precoVenda;
	int anoAquisicao, anoVenda;
	
	
	/**
	 * @param matricula
	 * @param marca
	 * @param modelo
	 * @param cor
	 * @param fuel
	 * @param username
	 * @param cilindrada
	 * @param precoAquisicao
	 * @param percoVenda
	 * @param anoAquisicao
	 * @param anoVenda
	 */
	
	public String toString() {
		return marca +" "+modelo+" - "+matricula;
	}
	
	public Car(String matricula, String marca, String modelo, String cor, String fuel, String username, int cilindrada,
			double precoAquisicao, double precoVenda, int anoAquisicao, int anoVenda) {
		super();
		this.matricula = matricula;
		this.marca = marca;
		this.modelo = modelo;
		this.cor = cor;
		this.fuel= fuel;
		this.username = username;
		this.cilindrada = cilindrada;
		this.precoAquisicao = precoAquisicao;
		this.precoVenda = precoVenda;
		this.anoAquisicao = anoAquisicao;
		this.anoVenda = anoVenda;
	}
	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return matricula;
	}
	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	/**
	 * @return the marca
	 */
	public String getMarca() {
		return marca;
	}
	/**
	 * @param marca the marca to set
	 */
	public void setMarca(String marca) {
		this.marca = marca;
	}
	/**
	 * @return the modelo
	 */
	public String getModelo() {
		return modelo;
	}
	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	/**
	 * @return the cor
	 */
	public String getCor() {
		return cor;
	}
	/**
	 * @param cor the cor to set
	 */
	public void setCor(String cor) {
		this.cor = cor;
	}
	/**
	 * @return the fuel
	 */
	public String getFue() {
		return fuel;
	}
	/**
	 * @param fue the fue to set
	 */
	public void setFue(String fuel){
		this.fuel = fuel;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the cilindrada
	 */
	public int getCilindrada() {
		return cilindrada;
	}
	/**
	 * @param cilindrada the cilindrada to set
	 */
	public void setCilindrada(int cilindrada) {
		this.cilindrada = cilindrada;
	}
	/**
	 * @return the precoAquisicao
	 */
	public double getPrecoAquisicao() {
		return precoAquisicao;
	}
	/**
	 * @param precoAquisicao the precoAquisicao to set
	 */
	public void setPrecoAquisicao(double precoAquisicao) {
		this.precoAquisicao = precoAquisicao;
	}
	/**
	 * @return the percoVenda
	 */
	public double getPrecoVenda() {
		return precoVenda;
	}
	/**
	 * @param percoVenda the percoVenda to set
	 */
	public void setPercoVenda(double percoVenda) {
		this.precoVenda = percoVenda;
	}
	/**
	 * @return the anoAquisicao
	 */
	public int getAnoAquisicao() {
		return anoAquisicao;
	}
	/**
	 * @param anoAquisicao the anoAquisicao to set
	 */
	public void setAnoAquisicao(int anoAquisicao) {
		this.anoAquisicao = anoAquisicao;
	}
	/**
	 * @return the anoVenda
	 */
	public int getAnoVenda() {
		return anoVenda;
	}
	/**
	 * @param anoVenda the anoVenda to set
	 */
	public void setAnoVenda(int anoVenda) {
		this.anoVenda = anoVenda;
	}
	
	
	
}
