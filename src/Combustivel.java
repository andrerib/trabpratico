
public class Combustivel {

	int idFuel;
	String Fuel;
	
	public String toString(){
		return Fuel;
	}
	
	/**
	 * @param idFuel
	 * @param fuel
	 */
	public Combustivel(int idFuel, String fuel) {
		super();
		this.idFuel = idFuel;
		Fuel = fuel;
	}
	/**
	 * @return the idFuel
	 */
	public int getIdFuel() {
		return idFuel;
	}
	/**
	 * @param idFuel the idFuel to set
	 */
	public void setIdFuel(int idFuel) {
		this.idFuel = idFuel;
	}
	/**
	 * @return the fuel
	 */
	public String getFuel() {
		return Fuel;
	}
	/**
	 * @param fuel the fuel to set
	 */
	public void setFuel(String fuel) {
		Fuel = fuel;
	}
	
	
	
	
}
